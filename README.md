# 1. Få det att fungera

* Nodejs (https://nodejs.org/en/download/)
* MongoDB (https://www.mongodb.com/download-center)

# 2. Ladda ner dependencies
```bash
npm install

# Möjligvis
npm i -g nodemon
```

```bash
cd client
npm install
```

# 3. Kopiera .env
* Kopiera .env.example
* Döp om den nya till .env

# 4. Starta Webappen
* Starta backend:
 ```bash
npm run dev
```

* Starta frontend (öppna en ny terminal)
```bash
cd client
npm run serve
```

(Frontend borde på http://localhost:8080)