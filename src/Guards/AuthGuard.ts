import express, {Request, Response, NextFunction} from 'express';

import Token from '../inc/Token';
import Auth from '../inc/Auth';

export default class AuthGuard {

    static requiresAuth(req: Request, res: Response, next: NextFunction)
    {
        
        if (req.headers.authorization == undefined)
            return res.status(401).json({errors: ["Unauthorized"]});

        let token: any = Token.verify(req.headers.authorization);

        if (!token)
            return res.status(401).json({errors: ["Unauthorized"]});

        res.locals.auth = new Auth(token.user);

        next();
    }

}