import express, {Request, Response, NextFunction} from 'express';

import Token from '../inc/Token';
import Auth from '../inc/Auth';
import Permission from '../Models/Permission';

export default class PermissionGuard {

    static async requiresViewOrMore(req: Request, res: Response, next: NextFunction)
    {
        try {
            let permission = await Permission.findOne({user_id: res.locals.auth.id(), project_id: res.locals.project._id});
            if (permission == undefined)
                return res.status(401).json({errors: ["Unauthorized"]});
            
            if (permission.type == "view" || permission.type == "comment" || permission.type == "edit" || permission.type == "admin" || permission.type == "owner") {
                res.locals.permission = permission.type;
                next();
                return;
            }

            return res.status(401).json({errors: ["Unauthorized"]});
        } catch (error) {
            return res.status(401).json({errors: ["Unauthorized"]});
        }
    }

    static async requiresCommentOrMore(req: Request, res: Response, next: NextFunction)
    {
        try {
            let permission = await Permission.findOne({user_id: res.locals.auth.id(), project_id: res.locals.project._id});
            if (permission == undefined)
                return res.status(401).json({errors: ["Unauthorized"]});
            
            if (permission.type == "comment" || permission.type == "edit" || permission.type == "admin" || permission.type == "owner") {
                res.locals.permission = permission.type;
                next();
                return;
            }

            return res.status(401).json({errors: ["Unauthorized"]});
        } catch (error) {
            return res.status(401).json({errors: ["Unauthorized"]});
        }
    }

    static async requiresEditOrMore(req: Request, res: Response, next: NextFunction)
    {
        try {
            let permission = await Permission.findOne({user_id: res.locals.auth.id(), project_id: res.locals.project._id});
            
            if (permission == undefined)
                return res.status(401).json({errors: ["Unauthorized"]});
            
            if (permission.type == "edit" || permission.type == "admin" || permission.type == "owner") {
                res.locals.permission = permission.type;
                next();
                return;
            }

            return res.status(401).json({errors: ["Unauthorized"]});
        } catch (error) {
            return res.status(401).json({errors: ["Unauthorized"]});
        }
    }

    static async requiresAdminOrMore(req: Request, res: Response, next: NextFunction)
    {
        try {
            let permission = await Permission.findOne({user_id: res.locals.auth.id(), project_id: res.locals.project._id});
            
            if (permission == undefined)
                return res.status(401).json({errors: ["Unauthorized"]});
            
            if (permission.type == "admin" || permission.type == "owner") {
                res.locals.permission = permission.type;
                next();
                return;
            }

            return res.status(401).json({errors: ["Unauthorized"]});
        } catch (error) {
            return res.status(401).json({errors: ["Unauthorized"]});
        }
    }

    static async requiresOwner(req: Request, res: Response, next: NextFunction)
    {
        try {
            let permission = await Permission.findOne({user_id: res.locals.auth.id(), project_id: res.locals.project._id});
            if (permission == undefined)
                return res.status(401).json({errors: ["Unauthorized"]});
            
            if (permission.type == "owner") {
                res.locals.permission = permission.type;
                next();
                return;
            }

            return res.status(401).json({errors: ["Unauthorized"]});
        } catch (error) {
            return res.status(401).json({errors: ["Unauthorized"]});
        }
    }

}