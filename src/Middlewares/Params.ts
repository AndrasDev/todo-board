import express, {Request, Response, NextFunction} from 'express';
import Project from '../Models/Project';
import Board from '../Models/Board';
import Item from '../Models/Item';
import Comment from '../Models/Comment';
import Permission from '../Models/Permission';


export default class Params {

    static async getProject(req: Request, res: Response, next: NextFunction)
    {
        try {
            let permission = await Permission.findOne({project_id: req.params.project_id, user_id: res.locals.auth.id()});
            
            if (!permission) {
                return res.status(401).json({errors: ["Unauthorized"]});
            }

            let project = await Project.findOne({_id: req.params.project_id});
            if (!project) {
                //bad request
            }
            res.locals.project = project;
    
            next();
        } catch (error) {
            return res.status(401).json({errors: ["Unauthorized"]});
        }
    }

    static async getBoard(req: Request, res: Response, next: NextFunction)
    {
        try {
            let board = await Board.findOne({_id: req.params.board_id, project_id: req.params.project_id});
    
            if (!board) {
                return res.status(401).json({errors: ["Unauthorized"]});
            }
            
            res.locals.board = board;
    
            next();
        } catch (error) {
            return res.status(401).json({errors: ["Unauthorized"]});
        }
    }

    static async getItem(req: Request, res: Response, next: NextFunction)
    {
        try {
            let item = await Item.findOne({_id: req.params.item_id, board_id: req.params.board_id});
    
            if (!item) {
                return res.status(401).json({errors: ["Unauthorized"]});
            }
            
            res.locals.item = item;
    
            next();
        } catch (error) {
            return res.status(401).json({errors: ["Unauthorized"]});
        }
    }

    static async getComment(req: Request, res: Response, next: NextFunction)
    {
        try {
            let comment = await Comment.findOne({_id: req.params.comment_id, poster_id: res.locals.auth.id()});
    
            if (!comment) {
                return res.status(401).json({errors: ["Unauthorized"]});
            }
            
            res.locals.comment = comment;
    
            next();
        } catch (error) {
            return res.status(401).json({errors: ["Unauthorized"]});
        }
    }

    static async getPermission(req: Request, res: Response, next: NextFunction)
    {
        try {
            let permission = await Permission.findOne({user_id: req.params.user_id, project_id: res.locals.project._id});
    
            if (!permission) {
                //TODO: Update to "didnt exist"
                return res.status(401).json({errors: ["Unauthorized"]});
            }
            
            res.locals.permission = permission;
    
            next();
        } catch (error) {
            return res.status(401).json({errors: ["Unauthorized"]});
        }
    }
}