import express, {Application, Router} from 'express';
import AuthController from '../Controllers/AuthController';
import ProjectController from '../Controllers/ProjectController';
import AuthGuard from '../Guards/AuthGuard';
import Params from '../Middlewares/Params';
import BoardController from '../Controllers/BoardController';
import ItemController from '../Controllers/ItemController';
import CommentController from '../Controllers/CommentController';
import PermissionController from '../Controllers/PermissionController';
import PermissionGuard from '../Guards/PermissionGuard';

const router: Router = express.Router();

router.post('/auth/register', AuthController.register);
router.post('/auth/login', AuthController.login);


//Projects
router.get('/projects', AuthGuard.requiresAuth, ProjectController.index);
router.post('/projects', AuthGuard.requiresAuth, ProjectController.store);

router.get('/projects/:project_id', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresViewOrMore, ProjectController.show);
router.patch('/projects/:project_id', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresEditOrMore,  ProjectController.update);
router.delete('/projects/:project_id', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresEditOrMore,  ProjectController.destroy);

//Comments
router.get('/projects/:project_id/comments', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresViewOrMore, CommentController.indexProject);
router.post('/projects/:project_id/comments', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresCommentOrMore,  CommentController.storeProject);
router.delete('/projects/:project_id/comments/:comment_id', AuthGuard.requiresAuth, Params.getProject, Params.getComment, CommentController.destroy);

//Permissions
router.get('/projects/:project_id/permissions', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresViewOrMore, PermissionController.index);
router.get('/projects/:project_id/permissions/me', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresViewOrMore, PermissionController.me);

router.post('/projects/:project_id/permissions', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresAdminOrMore, PermissionController.store);

router.patch('/projects/:project_id/permissions/:user_id', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresAdminOrMore, Params.getPermission, PermissionController.update);
router.delete('/projects/:project_id/permissions/:user_id', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresAdminOrMore, Params.getPermission, PermissionController.destroy);

//Boards
router.get('/projects/:project_id/boards', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresViewOrMore,  BoardController.index);
router.post('/projects/:project_id/boards', AuthGuard.requiresAuth, Params.getProject, PermissionGuard.requiresEditOrMore, BoardController.store);

router.get('/projects/:project_id/boards/:board_id', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, PermissionGuard.requiresViewOrMore, BoardController.show);
router.patch('/projects/:project_id/boards/:board_id', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, PermissionGuard.requiresEditOrMore, BoardController.update);
router.delete('/projects/:project_id/boards/:board_id', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, PermissionGuard.requiresEditOrMore, BoardController.destroy);

//Comments
router.get('/projects/:project_id/boards/:board_id/comments', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, PermissionGuard.requiresViewOrMore, CommentController.indexBoard);
router.post('/projects/:project_id/boards/:board_id/comments', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, PermissionGuard.requiresCommentOrMore, CommentController.storeBoard);
router.delete('/projects/:project_id/boards/:board_id/comments/:comment_id', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, Params.getComment, CommentController.destroy);



//Items
router.get('/projects/:project_id/boards/:board_id/items', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, PermissionGuard.requiresViewOrMore, ItemController.index);
router.post('/projects/:project_id/boards/:board_id/items', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, PermissionGuard.requiresEditOrMore, ItemController.store);

router.get('/projects/:project_id/boards/:board_id/items/:item_id', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, Params.getItem, PermissionGuard.requiresViewOrMore,  ItemController.show);
router.patch('/projects/:project_id/boards/:board_id/items/:item_id', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, Params.getItem, PermissionGuard.requiresEditOrMore, ItemController.update);
router.delete('/projects/:project_id/boards/:board_id/items/:item_id', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, Params.getItem, PermissionGuard.requiresEditOrMore, ItemController.destroy);

//Comments
router.get('/projects/:project_id/boards/:board_id/items/:item_id/comments', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, Params.getItem, PermissionGuard.requiresViewOrMore, CommentController.indexItem);
router.post('/projects/:project_id/boards/:board_id/items/:item_id/comments', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, Params.getItem, PermissionGuard.requiresCommentOrMore, CommentController.storeItem);
router.delete('/projects/:project_id/boards/:board_id/items/:item_id/comments/:comment_id', AuthGuard.requiresAuth, Params.getProject, Params.getBoard, Params.getItem, Params.getComment, CommentController.destroy);

export default router;