import express, {Request, Response} from 'express';
import Validator from '../inc/Validator';

import User from '../Models/User';
import Auth from '../inc/Auth';

class AuthController {

    static async register(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            name: 'required|string|min:2|max:30',
            username: 'required|string|min:3|max:30|unique:User',
            email: 'required|string|email|unique:User',
            password: 'required|string|min:8',
        });

        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }

        let user = await User.create(validation.data);

        res.status(201).json({
            data: { user }
        });
    }

    static async login(req: Request, res: Response)
    {
        const token = await Auth.attempt(req.body.username, req.body.password);

        if (!token) {
            return res.status(401).json({errors: ["Invalid Credentials"]});
        }

        res.status(200).json({
            data: { access_token: token }
        });
    }

}

export default AuthController;