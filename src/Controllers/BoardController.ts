import express, {Request, Response} from 'express';
import Validator from '../inc/Validator';

import Board from '../Models/Board';
import Item from '../Models/Item';

export default class BoardController {

    static async index(req: Request, res: Response)
    {
        res.json({ data: await Board.find({project_id: res.locals.project._id})});
    }

    static async store(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            name: 'required|string|min:2',
            description: 'required|string'
        });

        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }

        let board = await Board.create({...validation.data, project_id: res.locals.project._id});

        res.json({data: {board}});
    }

    static async update(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            name: 'string|min:2',
            description: 'string'
        });

        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }

        if (validation.data.name != undefined)
            res.locals.board.name = validation.data.name;

        if (validation.data.description != undefined)
            res.locals.board.description = validation.data.description;

        let board = await res.locals.board.save();

        res.json({data: {board}});
    }

    static async show(req: Request, res: Response)
    {
        let items = await Item.find({board_id: res.locals.board._id});
        res.json({data: { board: res.locals.board, items }});
    }

    static async destroy(req: Request, res: Response)
    {
        let { board } = res.locals;
        await board.remove();

        res.json({data: { board }});
    }
}