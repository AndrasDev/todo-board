import express, {Request, Response} from 'express';
import Validator from '../inc/Validator';

import Board from '../Models/Board';
import Item from '../Models/Item';
import Comment from '../Models/Comment';
import User from '../Models/User';

export default class CommentController {

    static async indexProject(req: Request, res: Response)
    {
        let comments = await Comment.find({project_id: res.locals.project._id}).sort([['created_at', -1]]).populate("poster_id").exec();

        res.json({ data: comments});
    }

    static async indexBoard(req: Request, res: Response)
    {
        let comments = await Comment.find({board_id: res.locals.board._id}).sort([['created_at', -1]]).populate("poster_id").exec();

        res.json({ data: comments});
    }

    static async indexItem(req: Request, res: Response)
    {        
        let comments = await Comment.find({item_id: res.locals.item._id}).sort([['created_at', -1]]).populate("poster_id").exec();

        res.json({ data: comments});
    }


    static async storeProject(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            text: 'required|string|min:1',
        });
        
        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }
        
        let comment = await Comment.create({...validation.data, poster_id: res.locals.auth.id(), project_id: res.locals.project._id});
        
        res.json({data: {comment}});
    }

    static async storeBoard(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            text: 'required|string|min:1',
        });
        
        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }
        
        let comment = await Comment.create({...validation.data, poster_id: res.locals.auth.id(), board_id: res.locals.board._id});
        
        res.json({data: {comment}});
    }

    static async storeItem(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            text: 'required|string|min:1',
        });
        
        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }
        
        let comment = await Comment.create({...validation.data, poster_id: res.locals.auth.id(), item_id: res.locals.item._id});
        
        res.json({data: {comment}});
    }

    static async destroy(req: Request, res: Response)
    {
        let { comment } = res.locals;
        await comment.remove();

        res.json({data: { comment }});
    }
}