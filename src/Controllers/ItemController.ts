import express, {Request, Response} from 'express';
import Validator from '../inc/Validator';

import Item from '../Models/Item';

export default class ItemController {

    static async index(req: Request, res: Response)
    {
        res.json({ data: await Item.find({board_id: res.locals.board._id})});
    }

    static async store(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            name: 'required|string|min:2',
            description: 'required|string',
            state: 'required|string'
        });

        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }

        let item = await Item.create({...validation.data, board_id: res.locals.board._id, project_id: res.locals.project._id});

        res.json({data: {item}});
    }

    static async update(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            name: 'string|min:2',
            description: 'string',
            state: 'required|string'
        });

        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }

        if (validation.data.name != undefined)
            res.locals.item.name = validation.data.name;

        if (validation.data.description != undefined)
            res.locals.item.description = validation.data.description;

        if (validation.data.state != undefined)
            res.locals.item.state = validation.data.state;

        let item = await res.locals.item.save();

        res.json({data: {item}});
    }

    static async show(req: Request, res: Response)
    {
        res.json({data: { item: res.locals.item }});
    }

    static async destroy(req: Request, res: Response)
    {
        let { item } = res.locals;
        await item.remove();
        await Item.deleteOne({_id: res.locals.item._id});

        res.json({data: { item }});
    }
}