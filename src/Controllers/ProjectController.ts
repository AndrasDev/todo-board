import express, {Request, Response} from 'express';
import Validator from '../inc/Validator';

import Project from '../Models/Project';
import Board from '../Models/Board';
import Item from '../Models/Item';
import Permission from '../Models/Permission';
import mongoose from 'mongoose';

export default class ProjectController {

    static async index(req: Request, res: Response)
    {
        let projects = await Permission.aggregate([
            {
                $match: { user_id: mongoose.Types.ObjectId(res.locals.auth.id()) }
            },
            {
                $lookup: {
                    from: "projects",
                    localField: "project_id",
                    foreignField: "_id",
                    as: "project"
                }
            },
            { $unwind: "$project" },
            { $addFields: { "project.permission": "$type" } },
            { $replaceRoot: { newRoot: "$project" } },
            { $sort : { updatedAt : -1 } }
        ]).exec();

        res.json({ data: projects });
    }

    static async store(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            name: 'required|string|min:2',
            description: 'required|string'
        });

        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }

        let project = await Project.create({...validation.data, owner_id: res.locals.auth.id()})

        res.json({data: {project}});
    }

    static async update(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            name: 'string|min:2',
            description: 'string'
        });

        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }

        if (validation.data.name != undefined)
            res.locals.project.name = validation.data.name;

        if (validation.data.description != undefined)
            res.locals.project.description = validation.data.description;

        let project = await res.locals.project.save();

        res.json({data: {project}});
    }

    static async show(req: Request, res: Response)
    {        
        let boards = await Board.find({project_id: res.locals.project._id});

        let boardsArray: Array<any> = [];

        for (let i = 0; i < boards.length; i++) {
            const board = boards[i];
            
            boardsArray.push({
                ...board.toJSON(),
                items: (await Item.find({board_id: board._id}))
            });
        }
        res.json({data: { project: { ...res.locals.project.toJSON(), permission: res.locals.permission, user_id: res.locals.auth.id(), boards: boardsArray } }});
    }

    static async destroy(req: Request, res: Response)
    {
        let { project } = res.locals;

        await project.remove();

        res.json({data: { project }});
    }
}