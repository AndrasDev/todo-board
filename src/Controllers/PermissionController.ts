import express, {Request, Response} from 'express';
import Validator from '../inc/Validator';
import mongoose from 'mongoose';

import Item from '../Models/Item';
import Permission from '../Models/Permission';
import User from '../Models/User';

export default class PermissionController {

    static async index(req: Request, res: Response)
    {
        let permissions = await Permission.aggregate([
            {
                $match: { project_id: mongoose.Types.ObjectId(res.locals.project._id) }
            },
            {
                $lookup: {
                    from: "users",
                    localField: "user_id",
                    foreignField: "_id",
                    as: "user"
                }
            },
            { $unwind: "$user" },
            { $addFields: { "user.permission": "$type" } },
            { $replaceRoot: { newRoot: "$user" } }
        ]).exec();

        res.json({ data: permissions });
    }

    static async me(req: Request, res: Response)
    {
        let permission = await Permission.findById(res.locals.auth.id());

        res.json({ data: {permission: permission?.type} });
    }

    static async store(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            name: 'required|string|min:2',
            permission: 'required|string'
        });

        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }

        try {
            let user = await User.findOne({$or: [ {username: validation.data.name}, {email: validation.data.name} ]});

            if (user == undefined) {
                return res.json({errors: ["Couldn't find a user with that Username or Email"]});
            }

            //TODO: Add so you can't have 2 or more of the same person in the permission
            let permission = await Permission.create({
                user_id: user._id,
                project_id: res.locals.project._id,
                type: validation.data.permission
            });

            res.json({data: {permission}});
        } catch (error) {
            return res.json({errors: ["Couldn't find a user with that Username or Email"]});
        }
    }

    static async update(req: Request, res: Response)
    {
        let validation = await Validator.validate(req.body, {
            permission: 'required|string'
        });

        if (!validation.valid) {
            return res.json({errors: validation.errors});
        }

        res.locals.permission.type = validation.data.permission;
        await res.locals.permission.save();

        res.json({data: {permission: res.locals.permission}});
    }

    static async destroy(req: Request, res: Response)
    {
        let { permission } = res.locals;

        await res.locals.permission.remove();

        res.json({data: { permission }});
    }
}