import mongoose, {Schema, Document} from 'mongoose';

export interface IComment extends Document {
    _id: mongoose.Types.ObjectId;
    poster_id: mongoose.Types.ObjectId;
    project_id: mongoose.Types.ObjectId;
    board_id: mongoose.Types.ObjectId;
    item_id: mongoose.Types.ObjectId;
    text: string;
    created_at: Date;
}

const commentSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    poster_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    project_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Project"
    },
    board_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Board"
    },
    item_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Item"
    },
    text: String,
    created_at: Date
  },
  { 
        versionKey: false
    }
);

commentSchema.pre('save', function (next: any) {

    let comment: any = this;

    if (comment._id == undefined)
        comment._id = new mongoose.Types.ObjectId();
        
    if (comment.created_at == undefined)
        comment.created_at = new Date(); 

    next();
});

export default mongoose.model<IComment>("Comment", commentSchema);
