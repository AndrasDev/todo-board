import mongoose, {Schema, Document} from 'mongoose';
import Item from './Item';
import Comment from './Comment';
import Project from './Project';

export interface IBoard extends Document {
    _id: mongoose.Types.ObjectId;
    project_id: mongoose.Types.ObjectId;
    name: string;
    description: string;
}

const boardSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    project_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Project"
    },
    name: String,
    description: String
  },
  { 
        versionKey: false
    }
);

boardSchema.pre<IBoard>('save', function (next: any) {

    let board: any = this;

    if (board._id == undefined)
        board._id = new mongoose.Types.ObjectId()    

    Project.findByIdAndUpdate(this.project_id, { updatedAt: new Date() }).exec();

    next();
});

boardSchema.pre<IBoard>('remove', function (next: any) {

    Comment.find({board_id: this._id}).exec((error, comments) => {
        comments.forEach(comment => {
            comment.remove();
        });
    });
    
    Item.find({board_id: this._id}).exec((error, items) => {
        items.forEach(item => {
            item.remove();
        });
    });
    
    Project.findByIdAndUpdate(this.project_id, { updatedAt: new Date() }).exec();
    
    next();
});

export default mongoose.model<IBoard>("Board", boardSchema);
