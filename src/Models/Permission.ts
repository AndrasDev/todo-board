import mongoose, {Schema, Document} from 'mongoose';
import Project from './Project';


export interface IPermission extends Document {
    _id: mongoose.Types.ObjectId;
    user_id: mongoose.Types.ObjectId;
    project_id: mongoose.Types.ObjectId;
    type: string;
}

const permissionSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    project_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Project"
    },
    type: String
    },
    { 
        versionKey: false
    }
);

permissionSchema.pre<IPermission>('save', function (next: any) {

    let permission: any = this;

    if (permission._id == undefined)
        permission._id = new mongoose.Types.ObjectId()    

    Project.findByIdAndUpdate(this.project_id, { updatedAt: new Date() }).exec();

    next();
});

permissionSchema.pre<IPermission>('removed', function (next: any) {

    let permission: any = this;

    if (permission._id == undefined)
        permission._id = new mongoose.Types.ObjectId()    

    Project.findByIdAndUpdate(this.project_id, { updatedAt: new Date() }).exec();

    next();
});

export default mongoose.model<IPermission>("Permission", permissionSchema);
