import mongoose, {Schema, Document} from 'mongoose';
import Comment from './Comment';
import Board from './Board';
import Permission from './Permission';

export interface IProject extends Document {
    _id: mongoose.Types.ObjectId;
    owner_id: mongoose.Types.ObjectId;
    name: string;
    description: string;
}

const projectSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    owner_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    name: String,
    description: String
  },
  { 
      timestamps: true,
        versionKey: false
    }
);

projectSchema.pre('save', function (next: any) {

    let project: any = this;

    if (project._id == undefined)
    {
        project._id = new mongoose.Types.ObjectId();
        
        Permission.create({
            project_id: this._id,
            user_id: project.owner_id,
            type: "owner"
        });

    }

    next();
});


projectSchema.pre('remove', function (next: any) {
    
    Comment.find({project_id: this._id}).exec((error, comments) => {
        comments.forEach(comment => {
            comment.remove();
        });
    });

    Board.find({project_id: this._id}).exec((error, boards) => {
        boards.forEach(board => {
            board.remove();
        });
    });

    Permission.deleteMany({project_id: this._id}).exec();

    next();
});

export default mongoose.model<IProject>("Project", projectSchema);
