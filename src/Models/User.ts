import mongoose, {Schema, Document} from 'mongoose';

import Security from '../inc/Security';

export interface IUser extends Document {
    username: string;
    name: string;
    email: string;
    password: string;
    avatar_url: string;
}

const userSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    username: String,
    email: String,
    password: String,
    avatar_url: String
  },
  { 
    versionKey: false,
    toJSON: {
        transform: function (doc, ret) {
          delete ret.password;
        }
      }
    }
);

userSchema.pre('save', function (next: any) {

    let user: any = this;

    user.name.trim();
    user.username.trim();
    user.email.trim();
    user.password.trim();

    if (user._id == undefined)
        user._id = new mongoose.Types.ObjectId()    

    if (user.avatar_url == undefined)
        user.avatar_url = "https://eu.ui-avatars.com/api/?name="+user.name;

    if (!user.isModified('password'))
        return next();

    if (user.password == undefined) {
        return "failed";
    }
    user.password = Security.hashPassword(user.password);
    next();
});

export default mongoose.model<IUser>("User", userSchema);
