import mongoose, {Schema, Document} from 'mongoose';
import Comment from './Comment';
import Project from './Project';

export interface IItem extends Document {
    _id: mongoose.Types.ObjectId;
    board_id: mongoose.Types.ObjectId;
    project_id: mongoose.Types.ObjectId;
    name: string;
    description: string;
    state: string;
}

const itemSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    board_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Board"
    },
    project_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Project"
    },
    name: String,
    description: String,
    state: String
  },
  { 
        versionKey: false
    }
);

itemSchema.pre<IItem>('save', function (next: any) {

    let item: any = this;

    if (item._id == undefined)
        item._id = new mongoose.Types.ObjectId()    

    Project.findByIdAndUpdate(this.project_id, { updatedAt: new Date() }).exec();


    next();
});

itemSchema.pre<IItem>('remove', function (next: any) {

    Comment.find({item_id: this._id}).exec((error, comments) => {
        comments.forEach(comment => {
            comment.remove();
        });
    });

    Project.findByIdAndUpdate(this.project_id, { updatedAt: new Date() }).exec();


    next();
});


export default mongoose.model<IItem>("Item", itemSchema);
