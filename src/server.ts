require('dotenv').config();

import express, {Application} from 'express';
import mongoose from 'mongoose';

import * as bodyParser from 'body-parser';
const cors = require('cors');


const app: Application = express();

//Middleware
app.use(cors());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

mongoose.connect((process.env.MONGODB_URI || ""), {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, (err) => {
    
    if (err) {
        console.log(err);
    	return;
    }
    
    console.log("Connected to Database");
});

//Init Models
import User from './Models/User';
User.init();

import Project from './Models/Project';
Project.init();

import Board from './Models/Board';
Board.init();

import Item from './Models/Item';
Item.init();

import Comment from './Models/Comment';
Comment.init();

import Permission from './Models/Permission';
Permission.init();

//Routes
import APIRoutes from './routes/api';
app.use('/api/', APIRoutes);

app.listen(process.env.PORT || 8000, () => console.log(`Server running on Port ${process.env.PORT}`));