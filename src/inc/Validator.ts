import mongoose from 'mongoose';

class Validator {

    static async validate(obj: any, schema: any): Promise<ValidationResult>
    {
        let results = new ValidationResult();

        for (let x = 0; x < Object.keys(schema).length; x++) {
            const field = Object.keys(schema)[x];

            let rules = schema[field].split('|');
            
            for (let i = 0; i < rules.length; i++) {
                const rule = rules[i];

                if (rule == "required") {                    
                    if (obj[field] == undefined) {
                        results.valid = false;
                        results.errors[field] = field + " is required";
                        break;
                    }
                }

                if (obj[field] == undefined) {
                    break;
                }

                if (rule == "string") {
                    if (typeof(obj[field]) != "string") {
                        results.valid = false;

                        results.errors[field] = field + " must be a string";
                        break;
                    }
                }

                if (rule == "email") {
                    if (!this.isEmail(obj[field])) {
                        results.valid = false;
                        results.errors[field] = field + " must be a valid email";
                        break;
                    }
                }

                let params = rule.split(':');

                if (params[0] == "min") {                    
                    if (this.min(obj[field], +params[1])) {
                        results.valid = false;
                        results.errors[field] = field + " is too short";
                        break;
                    }
                }

                if (params[0] == "max") {
                    if (this.max(obj[field], +params[1])) {
                        results.valid = false;
                        results.errors[field] = field + " is too long";
                        break;
                    }
                }

                if (params[0] == "unique") {
                    if (!(await this.unique(obj[field], params[1], field))) {
                        results.valid = false;
                        results.errors[field] = field + " is already in use";
                        break;
                    }
                }
            }

            results.data[field] = obj[field];
        }

        return results;
    }

    static isEmail(email: string): boolean
    {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    static min(value: any, min: number): boolean
    {
        if (typeof(value) == "string")
            return value.length < min;
        else
            return value < min;
    }

    static max(value: any, max: number): boolean
    {
        if (typeof(value) == "string")
            return value.length > max;
        else
            return value > max;
    }

    static async unique(value: any, model: string, field: string): Promise<boolean>
    {        
        let query: any = {};
        query[field] = value;

        return (await mongoose.model(model).countDocuments(query)) == 0;
    }

}

class ValidationResult {
    valid: boolean = true;
    errors: any = {};
    data: any = {}; 
}

export default Validator;