import * as jwt from 'jsonwebtoken';

export default class Token {

    static sign(data: any)
    {
        if (process.env.JWT_SECRET == undefined)
            throw "Missing JWT SECRET!";

        return jwt.sign(data, process.env.JWT_SECRET, {expiresIn: process.env.JWT_TTL});
    }

    static verify(token: string)
    {
        if (process.env.JWT_SECRET == undefined)
            throw "Missing JWT SECRET!";
            
        try {
            return jwt.verify(token, process.env.JWT_SECRET);
        } catch (error) {
            return false;
        }
    }


}