import * as bcrypt from 'bcryptjs';

class Security {

    static hashPassword(password: string): string
    {
        let rounds = 12;

        if (process.env.HASH_SALT != undefined)
            rounds = +process.env.HASH_SALT;

        const salt = bcrypt.genSaltSync(rounds);

        return bcrypt.hashSync(password, salt);
    }

    static comparePassword(hash: string, password: string): boolean
    {
        return bcrypt.compareSync(password, hash);
    }

}

export default Security;