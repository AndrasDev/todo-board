import User from '../Models/User';
import Security from './Security';

import Token from './Token';

export default class Auth {

    user_data: any;

    constructor(user: any)
    {
        this.user_data = user;
    }

    async user()
    {
        return await User.findById(this.user_data._id); 
    }

    id()
    {
        return this.user_data._id;
    }

    static async attempt(username: string, password: string)
    {
        if (username == undefined || password == undefined)
            return false;

        let user = await User.findOne({username});
        
        if (user == undefined)
            return false;
        
        if (!Security.comparePassword(user.password, password))
            return false;

        return Token.sign({user: user.toJSON()});
    }

}