import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";

import moment from 'moment';

Vue.config.productionTip = false;

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('YYYY/MM/DD hh:mm')
  }
})

router.beforeEach((to, from, next) => { 
    
    if (to.meta.requiresAuth)
    {
        if (!store.getters.loggedIn)
            next({name: "Login"});
        else
            next();
    }
    else if (to.meta.requiresVisitor)
    {        
        if (store.getters.loggedIn)
            next({name: "Index"});
        else
            next();
    }
    else
        next();
});

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount("#app");
