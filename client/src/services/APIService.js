import axios from "axios";
import store from "../store";

const url = "http://localhost:8000/api";

class APIService {

    static post(suburl, data , headers = {})
    {
        return new Promise((resolve, reject) => {
                
                axios.post(url + suburl, data, {headers}).then((res) => {
                    resolve(res.data)

                }).catch((error) => {
                    console.log(error);
                    
                    reject(error);
                });
           
        });
    }

    static patch(suburl, data , headers = {})
    {
        return new Promise((resolve, reject) => {
                axios.patch(url + suburl, data, {headers}).then((res) => {
                    resolve(res.data)

                }).catch((error) => {
                    reject(error);
                });
           
        });
    }

    static delete(suburl, headers = {})
    {
        return new Promise((resolve, reject) => {
            axios.delete(url + suburl, {headers}).then((res) => {
                resolve(res.data)

            }).catch((error) => {
                reject(error);
            });
           
        });
    }

    static get(suburl, headers = {})
    {
        return new Promise((resolve, reject) => {
            axios.get(url + suburl, {headers}).then((res) => {
                resolve(res.data)
            }).catch((error) => {
                reject(error);
            });
           
        });
    }

    static authHeader()
    {
        return { 
            Authorization: store.getters.token
        }
    }

    //#region Auth endpoints
    static register(user)
    {
        return this.post("/auth/register", user);
    }

    static login(user)
    {
        return this.post("/auth/login", user);
    }

    static me()
    {
        return this.get('/me');
    }
    //#endregion

    //#region Project
    static getProjects()
    {
        return this.get('/projects', this.authHeader());
    }

    static storeProject(project)
    {
        return this.post('/projects', project, this.authHeader());
    }

    static getProjectById(id)
    {
        return this.get('/projects/'+id, this.authHeader());
    }
    static updateProject(id, project)
    {
        return this.patch('/projects/'+id, project, this.authHeader());
    }

    static deleteProject(id)
    {
        return this.delete('/projects/'+id, this.authHeader());
    }

    static getProjectComments(id)
    {
        return this.get('/projects/' + id + '/comments', this.authHeader());
    }

    static storeProjectComment(id, comment)
    {
        return this.post('/projects/' + id + '/comments', comment, this.authHeader());
    }

    static getProjectPermissions(id)
    {
        return this.get('/projects/' + id + '/permissions', this.authHeader());
    }

    static storeProjectPermission(id, data)
    {
        return this.post('/projects/' + id + '/permissions', data, this.authHeader());
    }

    static updateProjectPermission(id, user_id, data)
    {
        return this.patch('/projects/' + id + '/permissions/' + user_id, data, this.authHeader());
    }
    //#endregion

    //#region Board

    static storeBoard(project_id, board)
    {
        console.log(project_id);
        
        return this.post('/projects/' + project_id + "/boards", board, this.authHeader());
    }

    static updateBoard(project_id, board_id, board)
    {                
        return this.patch('/projects/' + project_id + "/boards/" + board_id, board, this.authHeader());
    }

    static deleteBoard(project_id, board_id, board)
    {                
        return this.delete('/projects/' + project_id + "/boards/" + board_id, this.authHeader());
    }

    static getBoardComments(project_id, board_id)
    {                
        return this.get('/projects/' + project_id + "/boards/" + board_id + "/comments", this.authHeader());
    }

    static storeBoardComment(project_id, board_id, comment)
    {                
        return this.post('/projects/' + project_id + "/boards/" + board_id + "/comments", comment, this.authHeader());
    }
    //#endregion

    
    //#region Item
    static storeItem(project_id, board_id, item)
    {        
        return this.post('/projects/' + project_id + "/boards/" + board_id + "/items", item, this.authHeader());
    }

    static updateItem(project_id, board_id, item_id, item)
    {                
        return this.patch('/projects/' + project_id + "/boards/" + board_id + "/items/" + item_id, item, this.authHeader());
    }

    static deleteItem(project_id, board_id, item_id)
    {                
        return this.delete('/projects/' + project_id + "/boards/" + board_id + "/items/" + item_id, this.authHeader());
    }

    static getItemComments(project_id, board_id, item_id)
    {                
        console.log(project_id, board_id, item_id)
        return this.get('/projects/' + project_id + "/boards/" + board_id + "/items/" + item_id + "/comments", this.authHeader());
    }

    static storeItemComment(project_id, board_id, item_id, comment)
    {                
        return this.post('/projects/' + project_id + "/boards/" + board_id + "/items/" + item_id + "/comments", comment, this.authHeader());
    }

    // static getProjectById(id)
    // {
    //     return this.get('/projects/'+id, this.authHeader());
    // }
    //#endregion

    //auth shit
}

export default APIService;
