import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        token: localStorage.getItem('access_token') || null
    },
    getters: {
        loggedIn(state) {
            return state.token !== null;
        },
        token(state) {
            return state.token;
        }
    },
    mutations: {
        setToken(state, data){
            state.token = data.token;
        },
        destroyToken(state){
            state.token = null;
        }
    },
    actions: {
        setToken(context, token)
        {
            localStorage.setItem('access_token', token);
            context.commit('setToken', token);
        },
        destroyToken(context)
        {
            if (context.getters.loggedIn) {
                localStorage.removeItem('access_token');
                context.commit('destroyToken');
            }
        }
    },
    modules: {}
});
