import Vue from "vue";
import VueRouter from "vue-router";

import Auth from "./auth/index";
import Index from "../components/Index";
import Projects from "../components/Projects";
import Project from "../components/Project";
import PageNotFound from "../components/PageNotFound";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "Index",
        component: Index
    },
    {
        path: "/projects",
        name: "Projects",
        component: Projects,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/project/:_id",
        name: "Project.Show",
        component: Project,
        meta: {
            requiresAuth: true
        }
    },
    ...Auth,
    {
        path: "*",
        component: PageNotFound
    }
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});



export default router;
