import Login from "../../components/auth/Login.vue";
import Register from "../../components/auth/Register.vue";
import Logout from "../../components/auth/Logout.vue";
import Account from "../../components/auth/Account.vue";

export default [
    {
        path: "/auth/login",
        name: "Login",
        component: Login,
        meta: {
            requiresVisitor: true
        }
    },
    {
        path: "/auth/register",
        name: "Register",
        component: Register,
        meta: {
            requiresVisitor: true
        }
    },
    {
        path: "/auth/account",
        name: "Account",
        component: Account,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/auth/logout",
        name: "Logout",
        component: Logout,
        meta: {
            requiresAuth: true
        }
    }
];
